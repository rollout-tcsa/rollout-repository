<?php
$targetDirectory = '/var/www/html/logs/';

if (isset($_POST["subfolder"]) && !empty($_POST["subfolder"])) {
    $subfolder = $_POST["subfolder"];
    $targetDirectory .= $subfolder . '/';
} else {
    die("Subpasta não especificada.");
}

if (!file_exists($targetDirectory)) {
    mkdir($targetDirectory, 0777, true);
}

$targetFile = $targetDirectory . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$fileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

if ($fileType !== 'log') {
    echo "Apenas arquivos .log são permitidos.";
    $uploadOk = 0;
}

// Verifica se o arquivo não está sendo enviado diretamente para a raiz /
if (strpos($targetFile, '/..') !== false || $targetFile == '/logs/' || $targetFile == '/logs') {
    echo "Upload para a pasta raiz não permitido.";
    $uploadOk = 0;
}

if ($uploadOk == 0) {
    echo "O arquivo não foi enviado.";
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
        echo "O arquivo " . htmlspecialchars(basename($_FILES["fileToUpload"]["name"])) . " foi enviado com sucesso para a subpasta '$subfolder'.";
    } else {
        echo "Desculpe, ocorreu um erro ao enviar o arquivo.";
    }
}
?>
